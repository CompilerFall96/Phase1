grammar Atalk;

@members{
    void print(String str){
       System.out.println(str);
    }
}


program:
	(actor | NEW_LINE)*
	{print("program");};

actor:
	ACTOR (name=ID) '<' INTEGER '>' NEW_LINE
	(receiver | state | NEW_LINE)*
	END NEW_LINE
	{ print("actor IDENTIFIED --------> " + $name.text);};

receiver:
	RECEIVER (name=ID) '(' (((argument ',')* argument)')' | ')')  NEW_LINE
	(statement | NEW_LINE)*
	END NEW_LINE
	{print("receiver IDENTIFIED --------> " + $name.text);};

state:
	global_declare (NEW_LINE)
	{print("state");};

argument:
	data_type ID
	{print("argument");};

global_declare:
	data_type ID (',' ID)*
	{print("global_declare");};

data_type:
	TYPE (array_bracket)?
	{print("data_type");};

array_bracket:
	('[' expression ']')+
	{print("array_bracket");};

local_declare:
	data_type (ID | ID '=' expression)
	(',' (ID | ID '=' expression))* 
	{print("local_declare");};
			
assignment:
	variable '=' expression
	{print("assignment");};
	
variable:
	ID (array_bracket)?
	{print("variable");};

statement:
	(local_declare | assignment | expression | conditional_statement | send_statement | loop_statement |
		scope_statement | write_statement | read_statement | QUIT | BREAK) NEW_LINE
	{print("statement");};

expression:
	array_init
	{print("expression");};

array_init:
	'{' ((array_init ',')* array_init) '}' | logical_or_expr
	{print("array_init");};

logical_or_expr:
	logical_and_expr ('or' logical_and_expr)*
	{print("logical_or_expr --------> or");};
	
logical_and_expr:
	relational_equal_expr ('and' relational_equal_expr)*
	{print("logical_and_expr --------> and");};

relational_equal_expr:
	relational_expr (op=('==' | '<>') relational_expr)*
	{print("relational_equal_expr " + (($op.text == null) ? "" : ("--------> " + $op.text)));};
	
relational_expr:
	additive_expr (op=('<' | '>') additive_expr)*
	{print("relational_expr " + (($op.text == null) ? "" : ("--------> " + $op.text)));};

additive_expr:
	multiplicative_expr (op=('+' | '-') multiplicative_expr)*
	{print("additive_expr " + (($op.text == null) ? "" : ("--------> " + $op.text)));};

multiplicative_expr:
	one_op_expr (op=('*' | '/') one_op_expr)*
	{print("multiplicative_expr " + (($op.text == null) ? "" : ("--------> " + $op.text)));};

one_op_expr:
	primary | (op=('not' | '-') expression)*
	{print("one_op_expr " + (($op.text == null) ? "" : ("--------> " + $op.text)));};
	
primary:
	'(' expression ')' | read_statement | variable | INTEGER | WORD | STRING
	{print("primary");};
	
arithmetic:
	('*'|'/'|'+'|'-') expression
	{print("arithmetic");};
	
logical:
	('and'|'or') expression
	{print("logical");};
	
relational:
	('<'|'>'|'=='|'<>') expression
	{print("relational");};

conditional_statement:
	IF  expression  NEW_LINE (statement)*
	(ELSEIF expression NEW_LINE (statement)*)*
	(ELSE NEW_LINE (statement)*)?
	END
	{print("conditional_statement");};

loop_statement:
	FOREACH ID IN (variable | array_init) NEW_LINE
	(statement)*
	END
	{print("loop_statement");};

send_statement:
	(SELF | SENDER | ID) '<<' ID '(' ((expression ',')* expression ')') | ')' 
	{print("send_statement");};
	
write_statement:
	WRITE '(' expression ')'
	{print("write_statement");};
	
read_statement:
	READ '(' INTEGER ')'
	{print("read_statement");};

scope_statement:
	BEGIN NEW_LINE
	(statement)*
	END
	{print("scope_statement");};


STRING:	'"' (~('\r' | '\n' | '"'))* '"' {print("STRING: " + getText());};
WORD: '\'' (~[\r\n]) '\'' {print("WORD: " + getText());};
INTEGER: ([0-9]+) {print("INTEGER: " + getText());};
NEW_LINE:	'\n'+;

TYPE: ('int'|'char') {print("TYPE: " + getText());};
RECEIVER:	'receiver' {print("RECEIVER: " + getText());};
ACTOR:	'actor' {print("ACTOR: " + getText());};
FOREACH:	'foreach' {print("FOREACH: " + getText());};
QUIT:	'quit' {print("QUIT: " + getText());};
ELSEIF:	'elseif' {print("ELSEIF: " + getText());};
IF:	'if' {print("IF: " + getText());};
BREAK:	'break' {print("BREAK: " + getText());};
SELF:	'self' {print("SELF: " + getText());};
SENDER:	'sender' {print("SENDER: " + getText());};
ELSE:	'else' {print("ELSE: " + getText());};
END:	'end' {print("END: " + getText());};
BEGIN:	'begin' {print("BEGIN: " + getText());};
IN:	'in' {print("IN: " + getText());};
WRITE:	'write' {print("WRITE: " + getText());};
READ:	'read' {print("READ: " + getText());};

COMMENT:	('\n#' | '#') ~[\r\n]* {print("COMMENT: " + getText());} ->skip;
ID: [\ba-zA-Z_]([a-zA-Z_0-9]*) {print("ID: " + getText());};
WHITE_SPACE:	(' '|'\t'|'\r') -> skip;
